# servicios-sa-g5

# Registro Clientes

## Endpoints

- Registrar nuevo cliente (POST): https://sa5-regcliente.herokuapp.com/servicio/registrocliente
  -  Ejemplo de entrada: 

            {
                "nombre": "Carlos",
                "apellido": "Canté",
                "foto": "Foto",
                "correo": "carlos@mail.com",
                "password": "1234"
            }


# Registro Proveedores

## Endpoints

- Registrar nuevo proveedor (POST): https://sa5-regproveedor.herokuapp.com/servicio/registroproveedor
  -  Ejemplo de entrada: 

            {
                "nombre": "Oscar",
                "apellido": "Hernandez",
                "foto": "Foto",
                "correo": "oscar@mail.com",
                "password": "1234"
            }

# Registro Productos

## Endpoints

- Registrar nuevo producto (POST): https://sa5-regproducto.herokuapp.com/servicio/registroproducto

  -  Ejemplo de entrada (NUEVOS PARAMETROS): 

            {
                "nombrecategoria": "Bebidas",
                "precio": 15,
                "stock": 400,
                "nombre": "Coca cola 2.5L",
                "descripcion": "Coca cola de 3 litros",
                "foto": "Foto",
                "proveedor": "juan@mail.com",
                
                "compradirecta": true,
                "subasta": true,
                "precioinicial": 5
            }

- Obtener los productos por proveedor (POST): https://sa5-regproducto.herokuapp.com/servicio/listaproductosproveedor

  -  Ejemplo de entrada: 

            {
                "proveedor": "juan@mail.com"
            }

- Eliminar un producto (POST): https://sa5-regproducto.herokuapp.com/servicio/elimiarproducto

  -  Ejemplo de entrada: 

            {
                "id": "123456"
            }

- Modificar un producto (POST): https://sa5-regproducto.herokuapp.com/servicio/modificarproducto

  -  Ejemplo de entrada (NUEVOS PARAMETROS): 

            {
                "id": "123456",
                "nombrecategoria": "Bebidas",
                "precio": 15,
                "stock": 400,
                "nombre": "Coca cola 2.5L",
                "descripcion": "Coca cola de 3 litros",
                "foto": "Nueva foto",
                "proveedor": "juan@mail.com",

                "compradirecta": true,
                "subasta": true,
                "precioinicial": 5
            }

# Registro Categorias

## Endpoints

- Registrar nueva categoria (POST): https://sa5-regcategoria.herokuapp.com/servicio/registrocategoria
  -  Ejemplo de entrada: 

            {
                "nombre": "Bebidas"
            }

- Obtener todas la categorias con sus productos (GET): https://sa5-regcategoria.herokuapp.com/servicio/listcategorias

- Obtener una categoria con sus productos (POST): https://sa5-regcategoria.herokuapp.com/servicio/listproductoscategoria
  - Ejemplo de entrada:
        
            {
                "nombre": "Bebidas"
            }

# Modificar Usuarios

## Endpoints

- Modificar los datos de un usuario (POST): https://sa5-modusuario.herokuapp.com/servicio/modificarusuario

  -  Ejemplo de entrada: 

            {
                "nombre": "Juan",
                "apellido": "Perez",
                "foto": "Foto",
                "correo": "juan@mail.com",
                "password": "4321"
            }

# Agregar Tarjeta

## Endpoints

- Agregar una tarjeta a un usuario (POST): https://sa5-agregartarjeta.herokuapp.com/servicio/agregartarjeta
  -  Ejemplo de entrada: 

            {
                "correo": "Juans@mail.com",
                "titular": "Juan Lopez",
                "numero": 5050505050,
                "vencimiento": "01/01/2025"
            }

- Eliminar una tarjeta a un usuario (POST): https://sa5-agregartarjeta.herokuapp.com/servicio/eliminartarjeta
  -  Ejemplo de entrada: 

            {
                "correo": "Juans@mail.com",
                "numerotarjeta": 5050505050
            }

# Login

## Endpoints

- Agregar una tarjeta a un usuario (POST): https://sa5-login.herokuapp.com/servicio/login
  -  Ejemplo de entrada: 

            {
                "correo": "carlos@mail.com",
                "password": "4321"
            }

# Ordenes

## Endpoints

- Crear una nueva orden (POST): https://sa5-ordenes.herokuapp.com/servicios/crearorden
  -  Ejemplo de entrada: 

            {
                "correo": "carlos@mail.com",
                "productos": [
                    {
                        "id": "612f1eb3deb999223c477156",
                        "cantidad": 5
                    },
                    {
                        "id": "612f1f62deb999223c477162",
                        "cantidad": 10
                    }
                ]
            }

- Obtener todas las ordenes (GET): https://sa5-ordenes.herokuapp.com/servicios/obtenertodaslasordenes

- Obtener las ordenes de 1 usuario (POST): https://sa5-ordenes.herokuapp.com/servicios/obtenerordenesusuario
  -  Ejemplo de entrada: 

            {
                "correo": "carlos@mail.com"
            }

- Modificar el estado de una orden (POST): https://sa5-ordenes.herokuapp.com/servicios/modificarestadoorden
  - Posibles estados (Solo ejemplos):
   
                Pendiente: Cuando es creada.
                Aceptada: Cuando el proveedor acepta la solicitud.
                Facturada: Cuando se asocia a una factura/boleta de compra.
                Despachada: Cuando los productos son despachados.
                Rechazada: Cuando el proveedor rechaza la orden de compra.
  -  Ejemplo de entrada: 

            {
                "id": "612f2394b3dab8a5f965064c",
                "estado": "Despachada"
            }

- Obtener datos de una orden para factura (POST): https://sa5-ordenes.herokuapp.com/servicios/obtenerdatosfactura
  -  Ejemplo de entrada (id de la orden): 

            {
                "id": "612f2394b3dab8a5f965064c", 
            }

# Carrito

## Endpoints

- Crear un nuevo carrito para un cliente (POST): https://sa5-carrito.herokuapp.com/carrito/nuevocarrito
  -  Ejemplo de entrada: 

            {
                "correo": "juan@mail.com"
            }

- Agregar productos a un carrito (POST): https://sa5-carrito.herokuapp.com/carrito/agregarproductosacarrito
  -  Ejemplo de entrada (id del carrito): 

            {
				"id": "61580221b29bc4d54eb22cd9",
				"productos": [
		           {
		               "id": "612f1eb3deb999223c477156",
		               "cantidad": 5
		           },
		           {
		               "id": "612f87d1d85efb0016147572",
		               "cantidad": 10
		           }
		       ]
			}

- Eliminar productos de un carrito (POST): https://sa5-carrito.herokuapp.com/carrito/eliminarproductocarrito
  -  Ejemplo de entrada (id del carrito): 

            {
				"id": "61580221b29bc4d54eb22cd9",
				"productos": [
		           {
		               "id": "612f1eb3deb999223c477156"
		           },
		           {
		               "id": "612f87d1d85efb0016147572"
		           }
		       ]
			}

- Eliminar un carrito (POST): https://sa5-carrito.herokuapp.com/carrito/eliminarcarrito
  -  Ejemplo de entrada (id del carrito): 

            {
				"id": "61580221b29bc4d54eb22cd9"
			}

- Pagar un carrito (POST): https://sa5-carrito.herokuapp.com/carrito/pagarcarrito
  -  Ejemplo de entrada (id del carrito): 

            {
				"id": "61580221b29bc4d54eb22cd9"
			}

- Obtener carritos de un cliente (POST): https://sa5-carrito.herokuapp.com/carrito/obtenercarritos
  -  Ejemplo de entrada: 

            {
				"correo": "juan@mail.com"
			}

# Favoritos

## Endpoints

- Agregar productos a favoritos de un cliente (POST): https://sa5-carrito.herokuapp.com/favoritos/agregarproductos
  -  Ejemplo de entrada: 

            {
				"correo": "juan@mail.com",
				"productos": [
		           {
		               "id": "6158a395cbd7f346c8d30fb4"
		           },
                   {
		               "id": "612f1eb3deb999223c477156"
		           },
                   {
		               "id": "612f1f62deb999223c477162"
		           },
                   {
		               "id": "612f87d1d85efb0016147572"
		           }
		       ]
			}

- Eliminar productos de favoritos de un cliente (POST): https://sa5-carrito.herokuapp.com/favoritos/eliminarproductos
  -  Ejemplo de entrada: 

            {
				"correo": "carlos@mail.com",
				"productos": [
		           {
		               "id": "612f87d1d85efb0016147572"
		           },
		           {
		               "id": "612f1eb3deb999223c477156"
		           }
		       ]
			}

- Obtener lista de favoritos de un cliente (POST): https://sa5-carrito.herokuapp.com/favoritos/obtenerfavoritos
  -  Ejemplo de entrada: 

            {
                "correo": "juan@mail.com"
            }

# Subastas

## Endpoints

- Realizar una oferta por un producto (POST): https://sa5-carrito.herokuapp.com/subasta/nuevaoferta
  -  Ejemplo de entrada (id del producto): 

            {
			    "id": "6158a395cbd7f346c8d30fb4", 
               	"oferta": 20, 
               	"correo": "julian@mail.com" 
			}

- Obtener listado de ofertas de un cliente (POST): https://sa5-carrito.herokuapp.com/subasta/obtenerofertasporcliente
  -  Ejemplo de entrada: 

            {
                "correo": "julian@mail.com"
            }

- Obtener listado de ofertas de un producto (POST): https://sa5-carrito.herokuapp.com/subasta/obtenerofertasporproducto
  -  Ejemplo de entrada (id del producto): 

            {
                "id": "6158a395cbd7f346c8d30fb4"
            }

- Obtener listado de ofertas que un cliente realizo por un producto (POST): https://sa5-carrito.herokuapp.com/subasta/obtenerofertasporproductoycliente
  -  Ejemplo de entrada (id del producto): 

            {
                "id": "6158a395cbd7f346c8d30fb4",
                "correo": "julian@mail.com"
            }